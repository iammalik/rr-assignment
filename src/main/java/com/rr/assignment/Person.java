package com.rr.assignment;

/**
 * @author osandstrom
 * Date: 2014-05-07 Time: 16:10
 */
public class Person {
  private final int age;
  private final String email;

  public Person(int age, String email) {
    this.age = age;
    this.email = email;
  }

  public int getAge() {
    return age;
  }

  public String getEmail() {
    return email;
  }
}
