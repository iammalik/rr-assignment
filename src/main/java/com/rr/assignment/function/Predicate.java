package com.rr.assignment.function;

/**
 * @author osandstrom
 */
public interface Predicate<T> {
  boolean test(T t);
}
